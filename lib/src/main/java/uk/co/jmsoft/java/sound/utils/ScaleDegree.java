/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package uk.co.jmsoft.java.sound.utils;

public enum ScaleDegree
{
    ONE("1",Interval.UNISON,Interval.UNISON),
    FLAT_TWO("b2",Interval.MINOR_2ND,Interval.MAJOR_7TH),
    TWO("2",Interval.MAJOR_2ND,Interval.MINOR_7TH),
    FLAT_THREE("b3",Interval.MINOR_3RD,Interval.MAJOR_6TH),
    THREE("3",Interval.MAJOR_3RD,Interval.MINOR_6TH),
    FOUR("4",Interval.PERFECT_4TH,Interval.PERFECT_5TH),
    FLAT_FIVE("b5",Interval.TRITONE,Interval.TRITONE),
    FIVE("5",Interval.PERFECT_5TH,Interval.PERFECT_4TH),
    FLAT_SIX("b6",Interval.MINOR_6TH,Interval.MAJOR_3RD),
    SIX("6",Interval.MAJOR_6TH,Interval.MINOR_3RD),
    FLAT_SEVEN("b7",Interval.MINOR_7TH,Interval.MAJOR_2ND),
    SEVEN("7",Interval.MAJOR_7TH,Interval.MINOR_2ND);
    
    private String readableName;
    
    private Interval intervalBelow;
    
    private Interval intervalAbove;

    private ScaleDegree(String readableName, Interval intervalAbove, Interval intervalBelow)
    {
        this.readableName = readableName;
        this.intervalBelow = intervalBelow;
        this.intervalAbove = intervalAbove;
    }
    
    public String getReadableName()
    {
        return readableName;
    }

    public Interval getIntervalBelow()
    {
        return intervalBelow;
    }

    public Interval getIntervalAbove()
    {
        return intervalAbove;
    }
    /**
     * Returns the scale degree of the given degree note relative to the given root note.
     * @param root
     * @param degree
     * @return 
     */
    public static ScaleDegree getScaleDegree(Note root, Note degree)
    {
        ScaleDegree result;
        if(root.ordinal() < degree.ordinal())
        {
            int diff = degree.ordinal() - root.ordinal();
            return ScaleDegree.values()[diff % 12];
        }
        else
        {
            int diff = (root.ordinal() - degree.ordinal()) % 12;
            return ScaleDegree.values()[(12 - diff) % 12];
        }
    }
}
