/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Represents the step pattern of scale degrees in a scale or key.
 */
public class StepPattern
{

    public static final StepPattern MAJOR;
    public static final StepPattern MINOR;
    // TODO add more convenience step patterns

    static
    {

        List<ScaleDegree> majorSteps = new ArrayList<>(Arrays.asList(new ScaleDegree[]
        {
            ScaleDegree.ONE, ScaleDegree.TWO, ScaleDegree.THREE, ScaleDegree.FOUR, ScaleDegree.FIVE, ScaleDegree.SIX, ScaleDegree.SEVEN
        }));
        MAJOR = new StepPattern(majorSteps);

        List<ScaleDegree> minorSteps = new ArrayList<>(Arrays.asList(new ScaleDegree[]
        {
            ScaleDegree.ONE, ScaleDegree.TWO, ScaleDegree.FLAT_THREE, ScaleDegree.FOUR, ScaleDegree.FIVE, ScaleDegree.FLAT_SIX, ScaleDegree.FLAT_SEVEN
        }));
        MINOR = new StepPattern(minorSteps);
    }

    private final List<ScaleDegree> degrees;

    private final int stepCount;

    public StepPattern(List<ScaleDegree> degrees)
    {
        this.degrees = degrees;
        stepCount = degrees.size();
    }

    /**
     * Gets the scale degree at the given index. 1 is the root.
     *
     * @param degree - Must be greater than zero.
     * @return
     */
    public ScaleDegree getScaleDegree(int degreeIndex)
    {
        if (degreeIndex <= 0)
        {
            return null;
        }
        return degrees.get((degreeIndex - 1) % stepCount);
    }

    public int getStepCount()
    {
        return stepCount;
    }

    public List<ScaleDegree> getDegrees()
    {
        return new ArrayList<>(degrees);
    }

    /**
     * Returns the step pattern that is the mode of this step pattern at the
     * given scale degree. The root is scale degree .
     * <br><br>
     * Modes have the same step pattern as their parent step pattern but
     * rotated.
     *
     * @param modeDegree The scale degree the mode starts on. Must be between 1
     * and the number of steps in the pattern.
     * @return The new step pattern or null if <code>scaleDegree</code> < 1 or
     * > the number of scale steps.
     */
    public StepPattern getMode(int modeDegree)
    {
        if (modeDegree < 1 || modeDegree > stepCount)
        {
            return null;
        }

        // Adjust to zero indexing.
        modeDegree--;

        // The new step patterns ordinals are all offset from the originals by the same amount.
        int ordinalOffset = degrees.get(modeDegree).ordinal();

        List<ScaleDegree> result = new ArrayList<>();
        result.add(ScaleDegree.ONE);

        for (int degree = 1; degree < this.stepCount; degree++)
        {
            int nextDegree = degree + modeDegree;
            if (nextDegree >= stepCount)
            {
                nextDegree %= stepCount;
                ScaleDegree oldDegree = degrees.get(nextDegree);
                int stepOrdinal = oldDegree.ordinal() + 12 - ordinalOffset;
                result.add(ScaleDegree.values()[stepOrdinal]);
            }
            else
            {
                ScaleDegree oldDegree = degrees.get(nextDegree);
                int stepOrdinal = oldDegree.ordinal() - ordinalOffset;
                result.add(ScaleDegree.values()[stepOrdinal]);
            }
        }

        return new StepPattern(result);
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.degrees);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final StepPattern other = (StepPattern) obj;
        return Objects.equals(this.degrees, other.degrees);
    }

}
