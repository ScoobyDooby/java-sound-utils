/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package uk.co.jmsoft.java.sound.utils;

public enum NoteName
{
    C(KeyColour.WHITE, KeyAlignment.NONE, "C"),
    C_SHARP_D_FLAT(KeyColour.BLACK, KeyAlignment.LEFT, "C#/Db"),
    D(KeyColour.WHITE,KeyAlignment.NONE, "D"),
    D_SHARP_E_FLAT(KeyColour.BLACK, KeyAlignment.RIGHT, "D#/Eb"),
    E(KeyColour.WHITE, KeyAlignment.NONE, "E"),
    F(KeyColour.WHITE, KeyAlignment.NONE, "F"),
    F_SHARP_G_FLAT(KeyColour.BLACK, KeyAlignment.LEFT, "F#/Gb"),
    G(KeyColour.WHITE, KeyAlignment.NONE, "G"),
    G_SHARP_A_FLAT(KeyColour.BLACK, KeyAlignment.CENTER, "G#/Ab"),
    A(KeyColour.WHITE, KeyAlignment.NONE, "A"),
    A_SHARP_B_FLAT(KeyColour.BLACK, KeyAlignment.RIGHT, "A#/Bb"),
    B(KeyColour.WHITE, KeyAlignment.NONE, "B");

    private final KeyColour keyColour;
    private final KeyAlignment keyAlignment;
    private final String name;

    NoteName(KeyColour keyColour, KeyAlignment keyAlignment, String name)
    {
        this.keyColour = keyColour;
        this.keyAlignment = keyAlignment;
        this.name = name;
    }

    public NoteName getNoteNameAbove()
    {
        return getNoteNameAbove(this);
    }

    public NoteName getNoteNameBelow()
    {
        return getNoteNameBelow(this);
    }
    
    public NoteName getNoteNameRelative(int offset)
    {
        return getNoteNameRelative(this, offset);
    }

    /**
     * Get the alignment of the black key relative to its neighbouring white keys.
     * @return The alignment.
     */
    public KeyAlignment getKeyAlignment()
    {
        return keyAlignment;
    }

    public static NoteName getNoteNameAbove(NoteName noteName)
    {
        return getNoteNameRelative(noteName, 1);
    }

    public static NoteName getNoteNameBelow(NoteName noteName)
    {
        return getNoteNameRelative(noteName, -1);
    }
    
    /**
     * Gets the note name that is offset semitones above or below the given note.
     * @param noteName - The note name to find the new note name relative to.
     * @param offset - The offset in semitones between the two notes.
     * @return 
     */
    public static NoteName getNoteNameRelative(NoteName noteName, int offset)
    {
        int ordinal = noteName.ordinal();
        if(offset < 0)
        {
            offset = 12 + offset % 12;
        }
        
        int modOffset = offset % 12;
        int resultOrdinal = (ordinal + modOffset) % 12;
        return NoteName.values()[resultOrdinal];
    }

    /**
     * Returns the colour of the notes key on a piano keyboard.
     * @return The colour.
     */
    public KeyColour getKeyColour()
    {
        return keyColour;
    }

    /**
     * Returns the name of the note as human readable string.
     * @return - The name of the note.
     */
    public String getReadableName()
    {
        return name;
    }
    
    

    /**
     * The alignment of the notes key on a piano keyboard.
     */
    public enum KeyAlignment
    {
        LEFT,
        RIGHT,
        CENTER,
        NONE
    }

}
