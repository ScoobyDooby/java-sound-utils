/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

/**
 * Represents a musical interval.
 */
public enum Interval
{
    UNISON("Unison"),
    MINOR_2ND("Minor 2nd"),
    MAJOR_2ND("Major 2nd"),
    MINOR_3RD("Minor 3rd"),
    MAJOR_3RD("Major 3rd"),
    PERFECT_4TH("Perfect 4th"),
    TRITONE("Tritone"),
    PERFECT_5TH("Perfect 5th"),
    MINOR_6TH("Minor 6th"),
    MAJOR_6TH("Major 6th"),
    MINOR_7TH("Minor 7th"),
    MAJOR_7TH("Major 7th"),
    OCTAVE("Octave");
    
    private final String name;
    
    private Interval(String name)
    {
        this.name = name;
    }
    /**
     * Get human readable interval name.
     * @return The name of the interval.
     */
    public String getName()
    {
        return name;
    }
}
