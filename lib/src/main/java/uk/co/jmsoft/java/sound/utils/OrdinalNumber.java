package uk.co.jmsoft.java.sound.utils;

public class OrdinalNumber
{

    public static String getOrdinal(int number)
    {
        int lastDigit = number % 10;
        int last2Digits = number % 100;
        
        if(last2Digits > 10 && last2Digits < 20)
        {
            return String.valueOf(number) + "th";
        }
        
        switch (lastDigit)
        {
            case 1:
                return String.valueOf(number) + "st";

            case 2:
                return String.valueOf(number) + "nd";
                
            case 3:
                return String.valueOf(number) + "rd";

            default:
                return String.valueOf(number) + "th";

        }
    }
}
