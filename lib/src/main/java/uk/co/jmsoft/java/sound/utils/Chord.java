/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static uk.co.jmsoft.java.sound.utils.ScaleDegree.*;

/**
 * Represents a chord as a list of scale degrees above a root note.
 */
public class Chord
{

    /**
     * The major triad (1, 3, 5)
     */
    public static Chord MAJOR;

    /**
     * The minor triad (1, b3, 5)
     */
    public static Chord MINOR;

    /**
     * The diminished triad (1 b3 b5)
     */
    public static Chord DIMINISHED;

    /**
     * The augmented triad (1 3 #5)
     */
    public static Chord AUGMENTED;

    public static Chord MAJOR_7TH;

    public static Chord MINOR_7TH;

    public static Chord DOMINANT_7TH;

    /**
     * The diatonic diminished 7th (1 b3 b5 b7)
     */
    public static Chord HALF_DIMINISHED_7TH;

    static
    {
        MAJOR = new Chord(ONE, THREE, FIVE);
        MINOR = new Chord(ONE, FLAT_THREE, FIVE);
        DIMINISHED = new Chord(ONE, FLAT_THREE, FLAT_FIVE);
        AUGMENTED = new Chord(ONE, THREE, FLAT_SIX);
        MAJOR_7TH = new Chord(ONE, THREE, FIVE, SEVEN);
        MINOR_7TH = new Chord(ONE, FLAT_THREE, FIVE, FLAT_SEVEN);
        DOMINANT_7TH = new Chord(ONE, THREE, FIVE, FLAT_SEVEN);
        HALF_DIMINISHED_7TH = new Chord(ONE, FLAT_THREE, FLAT_FIVE, FLAT_SEVEN);
    }

    private List<ScaleDegree> degrees;

    public Chord(ScaleDegree... degrees)
    {
        this.degrees = new ArrayList<>(Arrays.asList(degrees));
    }

    public Chord(List<ScaleDegree> degrees)
    {
        this.degrees = new ArrayList<>(degrees);
    }

    public List<ScaleDegree> getDegrees()
    {
        return new ArrayList<>(degrees);
    }

    /**
     * Convenience method that returns the notes in a particular inversion of
     * the chord. Chord only have as many inversions as they have degrees.
     *
     * @param root The root note of the chord.
     * @param inversion The inversion to return. Must be between 0 and the
     * number of degrees in the chord. 0 is the root position.
     * @return A list with the inversion or null if inversion is greater than
     * the number of degrees in the chord, or the notes required to create the
     * chord are outside the range of notes provided by {@link Note}.
     */
    public List<Note> getInversion(Note root, int inversion)
    {
        if (inversion > degrees.size())
        {
            return null;
        }

        List<Note> result = new ArrayList<>();
        for (ScaleDegree eachDegree : degrees)
        {
            Optional<Note> note = root.getNoteAtOffset(eachDegree.ordinal());
            if (note.isEmpty())
            {
                return null;
            }
            result.add(note.get());
        }

        for (int i = 0; i < inversion; i++)
        {
            Optional<Note> inv = result.remove(0).getNoteAtIntervalAbove(Interval.OCTAVE);
            if (inv.isEmpty())
            {
                return null;
            }
            result.add(inv.get());
        }

        return result;
    }
}
