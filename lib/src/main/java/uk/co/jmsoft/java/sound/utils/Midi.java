/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package uk.co.jmsoft.java.sound.utils;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.ShortMessage;

/**
 *
 * @author johnm
 */
public class Midi
{
    /**
     * Convenience method for creating MIDI note on messages.Note that midi can only represent notes from C-1 to G9.
     * @param note The note must be between C-1 and G9.
     * @param channel The MIDI channel the message will be sent to.
     * @param velocity The note velocity. Between 0 and 127.
     * @return The message.
     * @throws javax.sound.midi.InvalidMidiDataException
     */
    public static ShortMessage getNoteOnMessage(Note note, int channel, int velocity) throws InvalidMidiDataException
    {
        return new ShortMessage(ShortMessage.NOTE_ON, channel, note.getMidiNumber(), velocity);
    }
    
     /**
     * Convenience method for creating MIDI note off messages.Note that midi can only represent notes from C-1 to G9.
     * @param note The note must be between C-1 and G9.
     * @param channel The MIDI channel the message will be sent to.
     * @param velocity The note velocity. Between 0 and 127.
     * @return The message.
     * @throws javax.sound.midi.InvalidMidiDataException
     */
    public static ShortMessage getNoteOffMessage(Note note, int channel, int velocity) throws InvalidMidiDataException
    {
        return new ShortMessage(ShortMessage.NOTE_OFF, channel, note.getMidiNumber(), velocity);
    }
}
