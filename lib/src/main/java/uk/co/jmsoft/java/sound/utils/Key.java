/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.List;

public class Key
{

    private String name;

    private final NoteName rootNote;
    private final StepPattern stepPattern;

    /**
     * Get the major key of the given root note.
     *
     * @param rootNote
     */
    public Key(NoteName rootNote)
    {
        this(rootNote.getReadableName() + " Major", rootNote, StepPattern.MAJOR);
    }

    /**
     * Create a key with the given step pattern and root note.
     *
     * @param name The human readable name of the key.
     * @param rootNote
     * @param stepPattern
     */
    public Key(String name, NoteName rootNote, StepPattern stepPattern)
    {
        this.name = name;
        this.rootNote = rootNote;
        this.stepPattern = stepPattern;
    }

    /**
     * Get the human readable name of the key.
     * @return 
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Get the note name of the scale degree. Degree 1 is the root.
     *
     * @param degree Must be greater than 0. 1 is the root.
     * @return The note name or null if degree is less than 1.
     */
    public NoteName getScaleDegreeNote(int degree)
    {
        if (degree < 1)
        {
            return null;
        }
        // convert to zero indexing
        degree--;
        return rootNote.getNoteNameRelative(stepPattern.getDegrees().get(degree % stepPattern.getStepCount()).ordinal());
    }

    /**
     * Get the name of the note in the key with the given scale degree.
     * <br><br>Does not need to be a scale degree in the key.
     *
     * @param degree
     * @return
     */
    public NoteName getScaleDegreeNote(ScaleDegree degree)
    {
        return rootNote.getNoteNameRelative(degree.ordinal());
    }

    public NoteName getRootNote()
    {
        return rootNote;
    }

    public StepPattern getType()
    {
        return stepPattern;
    }

    public List<ScaleDegree> getScaleDegrees()
    {
        return stepPattern.getDegrees();
    }

    /**
     * Get a list of the notes in the key from ascending from the root.
     *
     * @return
     */
    public List<NoteName> getNotes()
    {
        List<NoteName> result = new ArrayList<>();
        for (ScaleDegree eachDegree : getScaleDegrees())
        {
            result.add(rootNote.getNoteNameRelative(eachDegree.ordinal()));
        }
        return result;
    }
}
