/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Represents notes in scientific notation between C-1 and G9
 */
public enum Note implements Comparable<Note>
{
    C_MINUS_1(NoteName.C, -1),
    C_SHARP_D_FLAT_MINUS_1(NoteName.C_SHARP_D_FLAT,-1),
    D_MINUS_1(NoteName.D, -1),
    D_SHARP_E_FLAT_MINUS_1(NoteName.D_SHARP_E_FLAT,-1),
    E_MINUS_1(NoteName.E, -1),
    F_MINUS_1(NoteName.F, -1),
    F_SHARP_G_FLAT_MINUS_1(NoteName.F_SHARP_G_FLAT,-1),
    G_MINUS_1(NoteName.G, -1),
    G_SHARP_A_FLAT_MINUS_1(NoteName.G_SHARP_A_FLAT,-1),
    A_MINUS_1(NoteName.A, -1),
    A_SHARP_B_FLAT_MINUS_1(NoteName.A_SHARP_B_FLAT,-1),
    B_MINUS_1(NoteName.B, -1),
    C0(NoteName.C, 0),
    C_SHARP_D_FLAT_0(NoteName.C_SHARP_D_FLAT,0),
    D0(NoteName.D, 0),
    D_SHARP_E_FLAT_0(NoteName.D_SHARP_E_FLAT,0),
    E0(NoteName.E, 0),
    F0(NoteName.F, 0),
    F_SHARP_G_FLAT_0(NoteName.F_SHARP_G_FLAT,0),
    G0(NoteName.G, 0),
    G_SHARP_A_FLAT_0(NoteName.G_SHARP_A_FLAT,0),
    A0(NoteName.A, 0),
    A_SHARP_B_FLAT_0(NoteName.A_SHARP_B_FLAT,0),
    B0(NoteName.B, 0),
    C1(NoteName.C, 1),
    C_SHARP_D_FLAT_1(NoteName.C_SHARP_D_FLAT,1),
    D1(NoteName.D, 1),
    D_SHARP_E_FLAT_1(NoteName.D_SHARP_E_FLAT,1),
    E1(NoteName.E, 1),
    F1(NoteName.F, 1),
    F_SHARP_G_FLAT_1(NoteName.F_SHARP_G_FLAT,1),
    G1(NoteName.G, 1),
    G_SHARP_A_FLAT_1(NoteName.G_SHARP_A_FLAT,1),
    A1(NoteName.A, 1),
    A_SHARP_B_FLAT_1(NoteName.A_SHARP_B_FLAT,1),
    B1(NoteName.B, 1),
    C2(NoteName.C, 2),
    C_SHARP_D_FLAT_2(NoteName.C_SHARP_D_FLAT,2),
    D2(NoteName.D, 2),
    D_SHARP_E_FLAT_2(NoteName.D_SHARP_E_FLAT,2),
    E2(NoteName.E, 2),
    F2(NoteName.F, 2),
    F_SHARP_G_FLAT_2(NoteName.F_SHARP_G_FLAT,2),
    G2(NoteName.G, 2),
    G_SHARP_A_FLAT_2(NoteName.G_SHARP_A_FLAT,2),
    A2(NoteName.A, 2),
    A_SHARP_B_FLAT_2(NoteName.A_SHARP_B_FLAT,2),
    B2(NoteName.B, 2),
    C3(NoteName.C, 3),
    C_SHARP_D_FLAT_3(NoteName.C_SHARP_D_FLAT,3),
    D3(NoteName.D, 3),
    D_SHARP_E_FLAT_3(NoteName.D_SHARP_E_FLAT,3),
    E3(NoteName.E, 3),
    F3(NoteName.F, 3),
    F_SHARP_G_FLAT_3(NoteName.F_SHARP_G_FLAT,3),
    G3(NoteName.G, 3),
    G_SHARP_A_FLAT_3(NoteName.G_SHARP_A_FLAT,3),
    A3(NoteName.A, 3),
    A_SHARP_B_FLAT_3(NoteName.A_SHARP_B_FLAT,3),
    B3(NoteName.B, 3),
    C4(NoteName.C, 4),
    C_SHARP_D_FLAT_4(NoteName.C_SHARP_D_FLAT,4),
    D4(NoteName.D, 4),
    D_SHARP_E_FLAT_4(NoteName.D_SHARP_E_FLAT,4),
    E4(NoteName.E, 4),
    F4(NoteName.F, 4),
    F_SHARP_G_FLAT_4(NoteName.F_SHARP_G_FLAT,4),
    G4(NoteName.G, 4),
    G_SHARP_A_FLAT_4(NoteName.G_SHARP_A_FLAT,4),
    A4(NoteName.A, 4),
    A_SHARP_B_FLAT_4(NoteName.A_SHARP_B_FLAT,4),
    B4(NoteName.B, 4),
    C5(NoteName.C, 5),
    C_SHARP_D_FLAT_5(NoteName.C_SHARP_D_FLAT,5),
    D5(NoteName.D, 5),
    D_SHARP_E_FLAT_5(NoteName.D_SHARP_E_FLAT,5),
    E5(NoteName.E, 5),
    F5(NoteName.F, 5),
    F_SHARP_G_FLAT_5(NoteName.F_SHARP_G_FLAT,5),
    G5(NoteName.G, 5),
    G_SHARP_A_FLAT_5(NoteName.G_SHARP_A_FLAT,5),
    A5(NoteName.A, 5),
    A_SHARP_B_FLAT_5(NoteName.A_SHARP_B_FLAT,5),
    B5(NoteName.B, 5),
    C6(NoteName.C, 6),
    C_SHARP_D_FLAT_6(NoteName.C_SHARP_D_FLAT,6),
    D6(NoteName.D, 6),
    D_SHARP_E_FLAT_6(NoteName.D_SHARP_E_FLAT,6),
    E6(NoteName.E, 6),
    F6(NoteName.F, 6),
    F_SHARP_G_FLAT_6(NoteName.F_SHARP_G_FLAT,6),
    G6(NoteName.G, 6),
    G_SHARP_A_FLAT_6(NoteName.G_SHARP_A_FLAT,6),
    A6(NoteName.A, 6),
    A_SHARP_B_FLAT_6(NoteName.A_SHARP_B_FLAT,6),
    B6(NoteName.B, 6),
    C7(NoteName.C, 7),
    C_SHARP_D_FLAT_7(NoteName.C_SHARP_D_FLAT,7),
    D7(NoteName.D, 7),
    D_SHARP_E_FLAT_7(NoteName.D_SHARP_E_FLAT,7),
    E7(NoteName.E, 7),
    F7(NoteName.F, 7),
    F_SHARP_G_FLAT_7(NoteName.F_SHARP_G_FLAT,7),
    G7(NoteName.G, 7),
    G_SHARP_A_FLAT_7(NoteName.G_SHARP_A_FLAT,7),
    A7(NoteName.A, 7),
    A_SHARP_B_FLAT_7(NoteName.A_SHARP_B_FLAT,7),
    B7(NoteName.B, 7),
    C8(NoteName.C, 8),
    C_SHARP_D_FLAT_8(NoteName.C_SHARP_D_FLAT,8),
    D8(NoteName.D, 8),
    D_SHARP_E_FLAT_8(NoteName.D_SHARP_E_FLAT,8),
    E8(NoteName.E, 8),
    F8(NoteName.F, 8),
    F_SHARP_G_FLAT_8(NoteName.F_SHARP_G_FLAT,8),
    G8(NoteName.G, 8),
    G_SHARP_A_FLAT_8(NoteName.G_SHARP_A_FLAT,8),
    A8(NoteName.A, 8),
    A_SHARP_B_FLAT_8(NoteName.A_SHARP_B_FLAT,8),
    B8(NoteName.B, 8),
    C9(NoteName.C, 9),
    C_SHARP_D_FLAT_9(NoteName.C_SHARP_D_FLAT,9),
    D9(NoteName.D, 9),
    D_SHARP_E_FLAT_9(NoteName.D_SHARP_E_FLAT,9),
    E9(NoteName.E, 9),
    F9(NoteName.F, 9),
    F_SHARP_G_FLAT_9(NoteName.F_SHARP_G_FLAT,9),
    G9(NoteName.G, 9);
    

    private final NoteName noteName;
    private final int octave;

    private Note(NoteName noteName, int octave)
    {
        this.noteName = noteName;
        this.octave = octave;
    }

    public NoteName getNoteName()
    {
        return noteName;
    }

    public int getOctave()
    {
        return octave;
    }

    public String getReadableName()
    {
        return noteName.getReadableName() + octave;
    }
    /**
     * Get the note at the given semitone offset from this note.
     * @param offset
     * @return The note at the offset or empty if it is greater than G9.
     */
    public Optional<Note> getNoteAbove()
    {
        if (this != G9)
        {
            return Optional.ofNullable(Note.values()[this.ordinal() + 1]);
        }
        return Optional.empty();
    }
    /**
     * Get the note at the given semitone offset from this note.
     * @param offset
     * @return The note at the offset or empty if it is less than C-1.
     */
    public Optional<Note> getNoteBelow()
    {
        if(this != C_MINUS_1)
        {
            return Optional.ofNullable(Note.values()[this.ordinal() - 1]);
        }
        return Optional.empty();
    }
    /**
     * Get the note at the given semitone offset from this note.
     * @param offset
     * @return The note at the offset or empty if it is outside the range C-1 to G9.
     */
    public Optional<Note> getNoteAtOffset(int offset)
    {
        int newNoteOrdinal = this.ordinal() + offset;
        if(newNoteOrdinal > Note.values().length)
        {
            return Optional.empty();
        }
        return Optional.ofNullable(Note.values()[newNoteOrdinal]);
    }
    /**
     * Gets the note at the given interval above this note.
     * @param interval
     * @return The note at the interval or empty if the note is outside the range C-1 to G9.
     */
    public Optional<Note> getNoteAtIntervalAbove(Interval interval)
    {
        return getNoteAtOffset(interval.ordinal());
    }
    
    /**
     * Gets the note at the given interval below this note.
     * @param interval
     * @return The note at the interval or empty if the note is outside the range C-1 to G9.
     */
    public Optional<Note> getNoteAtIntervalBelow(Interval interval)
    {
        return getNoteAtOffset(-interval.ordinal());
    }

    /**
     * Get the MIDI note number for the given note. MIDI note 0 is C-1 and MIDI
     * note 127 is G9.
     *
     * @return The midi number of this note.
     */
    public int getMidiNumber()
    {
        // Formula is ordinal + (octave + 1) * 12
        int midiNumber = this.getNoteName().ordinal() + (this.getOctave() + 1) * 12;
        return midiNumber;
    }
    
        /**
     * Returns a list containing all the notes in the given range.
     * @param lowNote The lowest note in the range.
     * @param highNote The highest note in the range.
     * @return A list containing the range.
     */
    public static List<Note> getNoteRange(Note lowNote, Note highNote)
    {
        // Make sure range is ascending.
        if(lowNote.compareTo(highNote) > 0)
        {
            Note tempNote = lowNote;
            lowNote = highNote;
            highNote = tempNote;
        }
        
        List<Note> result = new ArrayList<>();
        
        Note currentNote = lowNote;
        result.add(currentNote);
        if(currentNote == highNote)
        {
            return result;
        }
        
        do
        {
            currentNote = currentNote.getNoteAbove().get();
            result.add(currentNote);
        }
        while (currentNote.compareTo(highNote) < 0);
        
        return result;
    }
}
