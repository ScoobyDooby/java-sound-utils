/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class ScaleDegreeTest
{
    
    public ScaleDegreeTest()
    {
    }
    

    /**
     * Test of getScaleDegree method, of class ScaleDegree.
     */
    @Test
    public void testGetScaleDegree()
    {
        System.out.println("getScaleDegree");
        // Degree above root
        Note root = Note.C4;
        Note degree = Note.D4;
        ScaleDegree expResult = ScaleDegree.TWO;
        ScaleDegree result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Degree below root
        root = Note.C4;
        degree = Note.B3;
        expResult = ScaleDegree.SEVEN;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Degree more than one octave above root
        root = Note.C4;
        degree = Note.E5;
        expResult = ScaleDegree.THREE;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Degree more than one octave below root
        root = Note.C4;
        degree = Note.F1;
        expResult = ScaleDegree.FOUR;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Degree and root same note
        root = Note.C4;
        degree = Note.C4;
        expResult = ScaleDegree.ONE;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Octave above
        root = Note.C4;
        degree = Note.C5;
        expResult = ScaleDegree.ONE;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
        // Octave below
        root = Note.C4;
        degree = Note.C3;
        expResult = ScaleDegree.ONE;
        result = ScaleDegree.getScaleDegree(root, degree);
        assertEquals(expResult, result);
        
    }
    
}
