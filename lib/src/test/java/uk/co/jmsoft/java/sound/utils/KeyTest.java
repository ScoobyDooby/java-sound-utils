package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class KeyTest
{

    public KeyTest()
    {
    }

    /**
     * Test of getScaleDegreeNote method, of class Key.
     */
//    @Test
//    public void testGetScaleDegreeNote()
//    {
//        System.out.println("getScaleDegreeNote");
//        int degree = 0;
//        Key instance = null;
//        NoteName expResult = null;
//        NoteName result = instance.getScaleDegreeNote(degree);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    @Test
    public void testGetMode()
    {
        StepPattern instance = StepPattern.MAJOR;
        StepPattern result = instance.getMode(3);
        List<ScaleDegree> degrees = new ArrayList<>(Arrays.asList(new ScaleDegree[]
        {
            ScaleDegree.ONE, ScaleDegree.FLAT_TWO, ScaleDegree.FLAT_THREE, ScaleDegree.FOUR, ScaleDegree.FIVE, ScaleDegree.FLAT_SIX, ScaleDegree.FLAT_SEVEN
        }));
        StepPattern expResult = new StepPattern(degrees);
        assertEquals(expResult, result);

        instance = StepPattern.MAJOR;
        result = instance.getMode(1);
        expResult = StepPattern.MAJOR;
        assertEquals(expResult, result);

        instance = StepPattern.MAJOR;
        result = instance.getMode(instance.getDegrees().size());
        assertNotNull(result);

        instance = StepPattern.MAJOR;
        result = instance.getMode(instance.getDegrees().size() + 1);
        assertNull(result);

        instance = StepPattern.MAJOR;
        result = instance.getMode(0);
        assertNull(result);
    }

    @Test
    public void testGetScaleDegreeNote()
    {
        Key instance = new Key(NoteName.C);
        NoteName result = instance.getScaleDegreeNote(2);
        NoteName expResult = NoteName.D;
        assertEquals(expResult, result);

        result = instance.getScaleDegreeNote(8);
        expResult = NoteName.C;
        assertEquals(expResult, result);

        result = instance.getScaleDegreeNote(9);
        expResult = NoteName.D;
        assertEquals(expResult, result);
    }

    @Test
    public void testGetNotes()
    {
        Key instance = new Key(NoteName.C);
        List<NoteName> result = instance.getNotes();
        List<NoteName> expResult = new ArrayList<>(Arrays.asList(new NoteName[]
        {
            NoteName.C, NoteName.D, NoteName.E, NoteName.F, NoteName.G, NoteName.A, NoteName.B
        }));
        
        assertEquals(expResult, result);
    }

}
