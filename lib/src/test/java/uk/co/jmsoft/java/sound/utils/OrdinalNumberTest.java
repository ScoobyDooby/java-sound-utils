/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package uk.co.jmsoft.java.sound.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class OrdinalNumberTest
{
    
    public OrdinalNumberTest()
    {
    }
  

    /**
     * Test of getOrdinal method, of class OrdinalNumber.
     */
    @Test
    public void testGetOrdinal()
    {
        System.out.println("getOrdinal");
        int number = 0;
        String expResult = "0th";
        String result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 1;
        expResult = "1st";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 2;
        expResult = "2nd";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 3;
        expResult = "3rd";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 4;
        expResult = "4th";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 10;
        expResult = "10th";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 11;
        expResult = "11th";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 19;
        expResult = "19th";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 21;
        expResult = "21st";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
        
        number = 12345;
        expResult = "12345th";
        result = OrdinalNumber.getOrdinal(number);
        assertEquals(expResult, result);
    }
    
}
