/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class NoteNameTest
{
    
    public NoteNameTest()
    {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception
    {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception
    {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception
    {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception
    {
    }

   

 

    /**
     * Test of getNoteNameAbove method, of class NoteName.
     */
    @org.junit.jupiter.api.Test
    public void testGetNoteNameAbove_0args()
    {
        System.out.println("getNoteNameAbove");
        NoteName instance = NoteName.A;
        NoteName expResult = NoteName.A_SHARP_B_FLAT;
        NoteName result = instance.getNoteNameAbove();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNoteNameBelow method, of class NoteName.
     */
    @org.junit.jupiter.api.Test
    public void testGetNoteNameBelow_0args()
    {
        System.out.println("getNoteNameBelow");
        NoteName instance = NoteName.C;
        NoteName expResult = NoteName.B;
        NoteName result = instance.getNoteNameBelow();
        assertEquals(expResult, result);
    }


    /**
     * Test of getNoteNameAbove method, of class NoteName.
     */
    @org.junit.jupiter.api.Test
    public void testGetNoteNameAbove_NoteName()
    {
        System.out.println("getNoteNameAbove");
        NoteName noteName = NoteName.E;
        NoteName expResult = NoteName.F;
        NoteName result = NoteName.getNoteNameAbove(noteName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNoteNameBelow method, of class NoteName.
     */
    @org.junit.jupiter.api.Test
    public void testGetNoteNameBelow_NoteName()
    {
        System.out.println("getNoteNameBelow");
        NoteName noteName = NoteName.C;
        NoteName expResult = NoteName.B;
        NoteName result = NoteName.getNoteNameBelow(noteName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNoteNameAbove method, of class NoteName.
     */
    @org.junit.jupiter.api.Test
    public void testGetNoteNameAbove_NoteName_int()
    {
        System.out.println("getNoteNameAbove");
        NoteName noteName = NoteName.A;
        int offset = -13;
        NoteName expResult = NoteName.G_SHARP_A_FLAT;
        NoteName result = NoteName.getNoteNameRelative(noteName, offset);
        assertEquals(expResult, result);
    }
    
}
