/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class ChordTest
{

    /**
     * Test of getInversion method, of class Chord.
     */
    @Test
    public void testGetInversion()
    {
        System.out.println("getInversion");
        // Root position
        Note root = Note.C4;
        int inversion = 0;
        Chord instance = Chord.MAJOR;
        List<Note> expResult = new ArrayList<>();
        expResult.add(Note.C4);
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.MAJOR_3RD).get());
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.PERFECT_5TH).get());
        List<Note> result = instance.getInversion(root, inversion);
        assertEquals(expResult, result);
        
        // First inversion
        root = Note.C4;
        inversion = 1;
        instance = Chord.MAJOR;
        expResult = new ArrayList<>();
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.MAJOR_3RD).get());
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.PERFECT_5TH).get());
        expResult.add(Note.C5);
        result = instance.getInversion(root, inversion);
        assertEquals(expResult, result);
        
        // Second inversion
        root = Note.C4;
        inversion = 2;
        instance = Chord.MAJOR;
        expResult = new ArrayList<>();
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.PERFECT_5TH).get());
        expResult.add(Note.C5);
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.MAJOR_3RD).get().getNoteAtIntervalAbove(Interval.OCTAVE).get());
        result = instance.getInversion(root, inversion);
        assertEquals(expResult, result);
        
        // Third inversion
        root = Note.C4;
        inversion = 3;
        instance = Chord.MAJOR;
        expResult = new ArrayList<>();
        expResult.add(Note.C5);
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.MAJOR_3RD).get().getNoteAtIntervalAbove(Interval.OCTAVE).get());
        expResult.add(Note.C4.getNoteAtIntervalAbove(Interval.PERFECT_5TH).get().getNoteAtIntervalAbove(Interval.OCTAVE).get());
        result = instance.getInversion(root, inversion);
        assertEquals(expResult, result);
        
        // Higher inversion than number of degrees should be null
        root = Note.C4;
        inversion = 4;
        instance = Chord.MAJOR;
        expResult = null;
        result = instance.getInversion(root, inversion);
        assertEquals(expResult, result);
        
        
    }

}
