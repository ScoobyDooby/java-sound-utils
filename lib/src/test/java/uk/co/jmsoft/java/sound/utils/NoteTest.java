/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.java.sound.utils;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author johnm
 */
public class NoteTest
{
    
    public NoteTest()
    {
    }
    
    @BeforeAll
    public static void setUpClass()
    {
    }
    
    @AfterAll
    public static void tearDownClass()
    {
    }
    
    @BeforeEach
    public void setUp()
    {
    }
    
    @AfterEach
    public void tearDown()
    {
    }

    /**
     * Test of getNoteName method, of class Note.
     */
    @Test
    public void testGetNoteName()
    {
        System.out.println("getNoteName");
        Note instance = Note.E0;
        NoteName expResult = NoteName.E;
        NoteName result = instance.getNoteName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOctave method, of class Note.
     */
    @Test
    public void testGetOctave()
    {
        System.out.println("getOctave");
        Note instance = Note.A2;
        int expResult = 2;
        int result = instance.getOctave();
        assertEquals(expResult, result);
    }


    /**
     * Test of getNoteAbove method, of class Note.
     */
    @Test
    public void testGetNoteAbove()
    {
        System.out.println("getNoteAbove");
        Note instance = Note.B0;
        Note expResult = Note.C1;
        Note result = instance.getNoteAbove().get();
        assertEquals(expResult, result);
        
        instance = Note.G9;
        Optional expResultEmpty = Optional.empty();
        Optional resultEmpty = instance.getNoteAbove();
        assertEquals(expResultEmpty, resultEmpty);
    }

    /**
     * Test of getNoteBelow method, of class Note.
     */
    @Test
    public void testGetNoteBelow()
    {
        System.out.println("getNoteBelow");
        Note instance = Note.C2;
        Note expResult = Note.B1;
        Note result = instance.getNoteBelow().get();
        assertEquals(expResult, result);
        
        instance = Note.C_MINUS_1;
        Optional expResultOptional = Optional.empty();
        Optional resultOptional = instance.getNoteBelow();
        assertEquals(expResultOptional, resultOptional);
    }
    
    @Test
    public void testGetMidiNumber()
    {
        System.out.println("getMidiNummber");
        Note instance = Note.C_MINUS_1;
        int expResult = 0;
        int result = instance.getMidiNumber();
        assertEquals(expResult, result);
        
        instance = Note.G9;
        expResult = 127;
        result = instance.getMidiNumber();
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of getNoteRange method, of class Utils.
     */
    @Test
    public void testGetNoteRange()
    {
        System.out.println("getNoteRange");
        Note lowNote = Note.A2;
        Note highNote = Note.B2;
        List<Note> result = Note.getNoteRange(lowNote, highNote);
        int expectedSize = 3;
        assertEquals(expectedSize, result.size());
    }
    
}
